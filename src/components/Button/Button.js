import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import Color from '../../constants/Color';
import CommonStyle from '../../constants/CommonStyle';
import CustomText from '../Text/Text';

const CustomButton = (props) => {
  const {label = 'defaultLabel', onPress, type = 'primary'} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        CommonStyle.shadow,
        type === 'danger' && {backgroundColor: Color.red},
      ]}>
      <CustomText bold style={styles.label}>
        {label}
      </CustomText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.blue,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
  label: {
    color: Color.white,
    fontSize: 18,
  },
});

export default CustomButton;
