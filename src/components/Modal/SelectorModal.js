import React, {useState, useImperativeHandle} from 'react';
import {
  Modal,
  TouchableOpacity,
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Color from '../../constants/Color';
import CommonStyle from '../../constants/CommonStyle';
import Metrics from '../../constants/Metrics';
import CustomText from '../Text/Text';

const SelectorModal = React.forwardRef((props, ref) => {
  const {deleteCar} = props;
  const [visible, setVisible] = useState(false);
  const [carList, setCarList] = useState([]);

  const show = (carList) => {
    setVisible(true);
    setCarList(carList);
  };

  const close = () => {
    setVisible(false);
    setCarList([]);
  };

  useImperativeHandle(ref, () => ({
    show,
    close,
  }));

  const deleteCarPressedHandler = (car) => {
    deleteCar(car);
    close();
  };

  return (
    <Modal animationType="fade" visible={visible} transparent>
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <CustomText bold>Lütfen silmek istediğiniz arabayı seçin</CustomText>
          <ScrollView style={styles.carList}>
            {carList.map((car) => {
              return (
                <TouchableOpacity
                  onPress={() => deleteCarPressedHandler(car)}
                  style={[styles.car, CommonStyle.shadow]}
                  key={car.id}>
                  <CustomText bold>
                    {car.plaka} ({car.color} - {car.brand})
                  </CustomText>
                  <CustomText>
                    {car.floor}
                    {car.place}
                  </CustomText>
                </TouchableOpacity>
              );
            })}
          </ScrollView>

          <View style={styles.bottomContainer}>
            <TouchableOpacity onPress={close}>
              <CustomText bold style={styles.close}>
                Kapat
              </CustomText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.transparentBlack,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    borderRadius: 10,
    padding: 16,
    width: Metrics.width * 0.8,
    height: Metrics.height * 0.5,
    backgroundColor: Color.white,
  },
  carList: {
    backgroundColor: Color.lightGray,
    borderRadius: 10,
    marginVertical: 10,
  },
  car: {
    backgroundColor: Color.white,
    margin: 10,
    borderRadius: 10,
    padding: 10,
  },
  bottomContainer: {alignSelf: 'flex-end'},
  close: {fontSize: 16, color: Color.black},
});

export default SelectorModal;
