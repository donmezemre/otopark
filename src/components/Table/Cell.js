import React from 'react';
import {StyleSheet, View} from 'react-native';
import Color from '../../constants/Color';
import commonHelper from '../../helper/commonHelper';
import CustomText from '../Text/Text';

const Cell = (props) => {
  const {place, index, leftHeader} = props;
  console.log('render cell', place);
  return (
    <View
      style={[
        styles.container,
        {backgroundColor: index % 2 === 1 ? Color.gray : Color.lightGray},
      ]}>
      {commonHelper.isEmpty(place) ? (
        leftHeader ? (
          <View>
            <CustomText bold>{index + 1}</CustomText>
          </View>
        ) : (
          <View>
            <CustomText>EMPTY</CustomText>
          </View>
        )
      ) : (
        <View>
          <CustomText bold>{place.plaka}</CustomText>
          <CustomText>{place.color}</CustomText>
          <CustomText>{place.brand}</CustomText>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    height: 70,
  },
});

export default React.memo(Cell);
