import React from 'react';
import {StyleSheet, View} from 'react-native';

import Cell from './Cell';

const Column = (props) => {
  const {column = {}, leftHeader = false} = props;

  return (
    <View style={[styles.container, leftHeader && {flex: 0}]}>
      {column.places.map((place, index) => {
        return (
          <Cell
            leftHeader={leftHeader}
            index={index}
            key={place.id || `${index}${column.floor}`}
            place={place}
          />
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Column;
