import React, {useMemo} from 'react';
import {View, StyleSheet} from 'react-native';
import Color from '../../constants/Color';
import CustomText from '../Text/Text';

import Column from './Column';

const Table = (props) => {
  const {data = []} = props;
  const leftHeaderData = useMemo(() => {
    return {places: [{}, {}, {}, {}, {}]};
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        {data.map((column, index) => {
          return (
            <View key={index} style={styles.headerText}>
              <CustomText bold>{column.floor} Katı</CustomText>
            </View>
          );
        })}
      </View>
      <View style={styles.columnContainer}>
        <Column leftHeader column={leftHeaderData} />
        {data.map((column, index) => {
          return <Column key={index.toString()} column={column} />;
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 16,
    marginTop: 0,
    flexDirection: 'column',
  },
  columnContainer: {
    flexDirection: 'row',
  },
  headerContainer: {
    backgroundColor: Color.gray,
    paddingLeft: 32,
    height: 45,
    flexDirection: 'row',
  },
  headerText: {
    flex: 1,
    padding: 10,
  },
});

export default Table;
