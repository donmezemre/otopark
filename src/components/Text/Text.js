import React from 'react';
import {StyleSheet, Text} from 'react-native';

const CustomText = (props) => {
  const {children, bold = false, style = {}} = props;
  return (
    <Text style={[styles.text, style, {fontWeight: bold ? 'bold' : 'normal'}]}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
  },
});

export default CustomText;
