import React, {memo, useRef} from 'react';
import {View, TextInput, StyleSheet, TouchableOpacity} from 'react-native';
import Color from '../../constants/Color';
import CommonStyle from '../../constants/CommonStyle';
import CustomText from '../Text/Text';

const CustomTextInput = (props) => {
  const {
    label = 'defaultLabel',
    placeholder = 'defaultPlaceholder',
    onChangeText,
    value = '',
  } = props;

  const textInput = useRef(null);

  const focusTextInput = () => {
    textInput.current.focus();
  };

  return (
    <TouchableOpacity
      onPress={focusTextInput}
      activeOpacity={1}
      style={[styles.container, CommonStyle.shadow]}>
      <View style={styles.labelContainer}>
        <CustomText bold style={styles.label}>
          {label}
        </CustomText>
      </View>
      <TextInput
        ref={textInput}
        onChangeText={onChangeText}
        placeholder={placeholder}
        value={value}
        style={styles.textInput}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.white,
    borderRadius: 8,
    padding: 8,
    paddingHorizontal: 16,
  },
  textInput: {
    padding: 0,
  },
  labelContainer: {
    marginBottom: 10,
  },
  label: {
    color: Color.blue,
    fontSize: 16,
  },
});

export default memo(CustomTextInput);
