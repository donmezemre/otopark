const Color = {
  black: '#000',
  transparentBlack: 'rgba(0,0,0,0.3)',
  white: '#FFF',
  gray: '#BDBDBD',
  lightGray: '#EFEFEF',
  blue: '#3272FE',
  red: '#F44336',
};

export default Color;
