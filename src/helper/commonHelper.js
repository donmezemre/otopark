const commonHelper = {
  isEmpty: (obj) => {
    return (
      Object.keys(obj).length === 0 &&
      (obj.constructor === Object || obj.constructor === Array)
    );
  },
};

export default commonHelper;
