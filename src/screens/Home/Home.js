import React, {useState, useCallback} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Keyboard,
} from 'react-native';
import CustomButton from '../../components/Button/Button';
import SelectorModal from '../../components/Modal/SelectorModal';
import Table from '../../components/Table/Table';
import CustomText from '../../components/Text/Text';
import CustomTextInput from '../../components/TextInput/TextInput';
import homeHelper from './homeHelper';

const Home = () => {
  const selectorRef = React.createRef();
  const [plaka, setPlaka] = useState('');
  const [color, setColor] = useState('');
  const [brand, setBrand] = useState('');
  const [data, setData] = useState([
    {
      floor: 'A',
      places: [{}, {}, {}, {}, {}],
    },
    {
      floor: 'B',
      places: [{}, {}, {}, {}, {}],
    },
    {
      floor: 'C',
      places: [{}, {}, {}, {}, {}],
    },
  ]);

  const addButtonPressedHandler = useCallback(() => {
    const isValid = homeHelper.checkForm(plaka, color, brand);
    if (!isValid) return;
    const newData = homeHelper.addCar(data, {plaka, color, brand});
    if (newData.hasOwnProperty('error')) {
      alert(newData.error);
    } else {
      setData(newData);
      setPlaka('');
      setColor('');
      setBrand('');
      Keyboard.dismiss();
    }
  }, [data, plaka, color, brand]);

  const deleteButtonPressedHandler = () => {
    const carList = homeHelper.getCarList(data);
    selectorRef.current.show(carList);
  };

  deleteCar = (car) => {
    const newData = homeHelper.deleteCar(data, car);
    setData(newData);
  };

  return (
    <KeyboardAvoidingView style={styles.container}>
      <SelectorModal deleteCar={deleteCar} ref={selectorRef} />
      <ScrollView keyboardShouldPersistTaps="handled">
        <View style={styles.titleContainer}>
          <CustomText bold style={{fontSize: 36}}>
            OTOPARK
          </CustomText>
        </View>
        <View style={styles.textInputContainer}>
          <CustomTextInput
            value={plaka}
            onChangeText={setPlaka}
            label="Plaka"
            placeholder="Lütfen plaka girin"
          />
        </View>
        <View style={styles.textInputContainer}>
          <CustomTextInput
            value={color}
            onChangeText={setColor}
            label="Renk"
            placeholder="Lütfen renk girin"
          />
        </View>
        <View style={styles.textInputContainer}>
          <CustomTextInput
            value={brand}
            onChangeText={setBrand}
            label="Aracın Markası"
            placeholder="Lütfen aracın markasını girin"
          />
        </View>
        <View style={styles.buttonContainer}>
          <CustomButton onPress={addButtonPressedHandler} label="Ekle" />
        </View>
        <View style={styles.buttonContainer}>
          <CustomButton
            type="danger"
            onPress={deleteButtonPressedHandler}
            label="Sil"
          />
        </View>
        <View style={styles.titleContainer}>
          <CustomText bold style={{fontSize: 36}}>
            ARAÇ LİSTESİ
          </CustomText>
        </View>
        <Table data={data} />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 8,
  },
  titleContainer: {
    padding: 16,
    paddingVertical: 8,
  },
  textInputContainer: {
    padding: 16,
    paddingVertical: 8,
  },
  buttonContainer: {
    padding: 6,
    paddingHorizontal: 16,
  },
});

export default Home;
