import {v4 as uuidv4} from 'uuid';
import commonHelper from '../../helper/commonHelper';

const formHelper = {
  checkForm: (plaka, color, brand) => {
    if (plaka.length === 0 || color.length === 0 || brand.length === 0) {
      alert('Bütün bilgileri girmelisin');
      return false;
    }
    return true;
  },

  addCar: (data, params) => {
    const newData = [...data];
    const newParams = {...params, id: uuidv4()};
    const isCarExist = formHelper.checkCarExists(data, params.plaka);

    if (isCarExist) return {error: 'Bu plakaya ait araba zaten park etmiş.'};

    for (let floor = 0; floor < data.length; floor++) {
      for (let place = 0; place < data[floor].places.length; place++) {
        if (commonHelper.isEmpty(data[floor].places[place])) {
          newData[floor].places[place] = newParams;
          return newData;
        }
      }
    }

    return {error: 'Heryer dolu!'};
  },

  checkCarExists: (data, plaka) => {
    for (let floor = 0; floor < data.length; floor++) {
      for (let place = 0; place < data[floor].places.length; place++) {
        if (data[floor].places[place]?.plaka === plaka) {
          return true;
        }
      }
    }
    return false;
  },

  getCarList: (data) => {
    const carList = [];
    for (let floor = 0; floor < data.length; floor++) {
      for (let place = 0; place < data[floor].places.length; place++) {
        if (!commonHelper.isEmpty(data[floor].places[place])) {
          const obj = {
            ...data[floor].places[place],
            floor: data[floor].floor,
            place: place + 1,
          };
          carList.push(obj);
        }
      }
    }
    return carList;
  },

  deleteCar: (data, car) => {
    const newData = [...data];
    for (let floor = 0; floor < data.length; floor++) {
      for (let place = 0; place < data[floor].places.length; place++) {
        if (data[floor].places[place].id === car.id) {
          newData[floor].places[place] = {};
          return newData;
        }
      }
    }
    return newData;
  },
};

export default formHelper;
